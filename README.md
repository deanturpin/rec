# RECOMMENDATIONS

## BARS AND COCKTAILS
Up the road Gung Ho! Tiny but great cocktail bar 👍🏼

Plotting Parlour, be sure to book!

Artist Residence down the road on Regency Square. Great cocktails and surroundings!

Browns Bar

Rockwater along Hove beach side, stylish bar/restaurant on 2 levels

## FOOD
Terre a Terre (long-running vegetarian restaurant)

Burnt Orange (small delicious plates)

Our fave place atm! 🥰

Coalshed (excellent steaks)

Riddle and Finn's, Due South and Murmur for lovely seafood on the beach

Saltroom, Etch and Cyan at The Grand, perfect for a special lunch/ dinner 💙

The Flint House is good, lovely food, small plates.
Also Kindling
Petis Pois
Issac At
The Little Fish Market. 
And the beautiful Ivy 🤩All excellent!

More relaxed is: Naninella, down the road, great pizza! 

Goemon for cheap, chilled Japanese cafe style, just down the road

If you like Thai Lucky Khao is great. Modern funky Thai!

Bincho is up the road a little and is a cool and delicious Japanese yakitori place

At the bottom of the road is New Club, nice for breakfast and  brunch/lunch.

Lovely, local bakery and cafe chain on the front, near the i360 The Flour Pot, great baked goods and coffee. 

If you need anything else do give me a shout!

Have a wonderful time! 😍😎
